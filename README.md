# README #

# App Development: Android #
## Fall 2015: Activity 1 ##

### Description ###

The objective of this assignment is to get students up-and-running in building simple
Android apps.

Follow the online tutorial Building Your First App on the Android developer website:
https://developer.android.com/training/basics/firstapp/index.html

The tutorial consists of the following four steps:

1. Creating an Android Project
2. Running Your Application
3. Building a Simple User Interface
4. Starting Another Activity